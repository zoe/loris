import 'package:flutter/material.dart';
import 'themes.dart' as themes;

themes.CustomColors theme = themes.CustomColors(
    "Dracula",
    const Color.fromARGB(
      255,
      98,
      114,
      164,
    ),
    const ColorScheme(
      brightness: Brightness.dark,
      primary: Color.fromARGB(255, 255, 121, 198),
      onPrimary: Color.fromARGB(255, 40, 42, 54),
      secondary: Color.fromARGB(255, 80, 250, 123),
      onSecondary: Color.fromARGB(255, 40, 42, 54),
      error: Color.fromARGB(255, 255, 85, 85),
      onError: Color.fromARGB(255, 40, 42, 54),
      background: Color.fromARGB(255, 40, 42, 54),
      onBackground: Color.fromARGB(255, 248, 248, 242),
      surface: Color.fromARGB(255, 68, 71, 90),
      onSurface: Color.fromARGB(255, 248, 248, 242),
    ));
