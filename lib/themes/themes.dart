import 'package:flutter/material.dart';
import 'dracula.dart' as color_dracula;

final available = [color_dracula.theme];
ThemeData getTheme(CustomColors colors) {
  return ThemeData(
    scaffoldBackgroundColor: colors.colorScheme.background,
    bottomAppBarColor: colors.colorScheme.background,
    hintColor: colors.hintColor,
    colorScheme: colors.colorScheme,
    errorColor: colors.colorScheme.error,
    bottomAppBarTheme: BottomAppBarTheme(
      color: colors.colorScheme.surface,
      shape: const CircularNotchedRectangle(),
      elevation: 0,
    ),
    navigationBarTheme: const NavigationBarThemeData(
      backgroundColor: Colors.transparent,
      labelBehavior: NavigationDestinationLabelBehavior.onlyShowSelected,
      indicatorColor: Colors.transparent,
      elevation: 0,
      height: 64,
    ),
    scrollbarTheme: const ScrollbarThemeData(),
  );
}

class CustomColors {
  late String name;
  late Color hintColor;
  late ColorScheme colorScheme;
  CustomColors(this.name, this.hintColor, this.colorScheme);
}
