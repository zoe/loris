import 'package:flutter/material.dart';

class MakePost extends StatelessWidget {
  const MakePost({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      backgroundColor: Theme.of(context).colorScheme.background,
      elevation: 0,
      contentPadding: const EdgeInsets.all(24),
      insetPadding: const EdgeInsets.all(24),
      children: [
        TextFormField(
          autofocus: true,
          keyboardType: TextInputType.multiline,
          minLines: 4,
          maxLines: null,
        ),
        const MakePostActionBar(),
      ],
    );
  }
}

class MakePostActionBar extends StatelessWidget {
  const MakePostActionBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        IconButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          icon: const Icon(Icons.cancel),
        ),
      ],
    );
  }
}
