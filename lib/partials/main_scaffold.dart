import 'package:flutter/material.dart';
import 'package:localization/localization.dart';
import 'package:slothmu/dialogues/makepost.dart';
import 'package:slothmu/pages/chat/chat.dart';
import 'package:slothmu/pages/notifications/notifications.dart';
import 'package:slothmu/pages/timeline/timeline.dart';
import 'package:slothmu/pages/settings/settings.dart';

class MainScaffold extends StatefulWidget {
  const MainScaffold({Key? key}) : super(key: key);

  @override
  State<MainScaffold> createState() => _MainScaffoldState();
}

class _MainScaffoldState extends State<MainScaffold> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    final screens = [
      const Timeline(),
      chat(context),
      notifications(context),
      settings(context),
    ];
    final buttons = [
      FloatingActionButton(
        child: const Icon(Icons.create),
        onPressed: () => showDialog(
          context: context,
          builder: (context) => const MakePost(),
        ),
      ),
      FloatingActionButton(
        onPressed: () {},
        child: const Icon(Icons.person_add),
      ),
      FloatingActionButton(
        onPressed: () {},
        child: const Icon(Icons.clear_all),
      ),
      null,
    ];
    return Scaffold(
      extendBody: true,
      body: screens[index],
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: buttons[index],
      bottomNavigationBar: BottomAppBar(
        child: NavigationBar(
            onDestinationSelected: (index) =>
                setState(() => this.index = index),
            selectedIndex: index,
            destinations: [
              NavigationDestination(
                  icon: const Icon(Icons.forum), label: "timeline".i18n()),
              NavigationDestination(
                  icon: const Icon(Icons.chat), label: "chat".i18n()),
              NavigationDestination(
                  icon: const Icon(Icons.notifications),
                  label: "notifications".i18n()),
              NavigationDestination(
                  icon: const Icon(Icons.settings), label: "settings".i18n()),
            ]),
      ),
    );
  }
}
