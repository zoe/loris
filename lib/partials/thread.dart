import 'package:flutter/material.dart';
import 'package:slothmu/partials/post.dart';

class Thread extends StatefulWidget {
  const Thread({Key? key}) : super(key: key);

  @override
  State<Thread> createState() => _ThreadState();
}

class _ThreadState extends State<Thread> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.all(4),
          child: Container(
            padding: const EdgeInsets.all(24),
            width: MediaQuery.of(context).size.width / 1.2,
            constraints: const BoxConstraints(maxWidth: 1000, minWidth: 375),
            decoration: BoxDecoration(
              color: Theme.of(context).colorScheme.surface,
              border:
                  Border.all(color: Theme.of(context).colorScheme.secondary),
              borderRadius: BorderRadius.circular(8),
            ),
            child: Column(
              children: [Post(), Post(), Post()],
            ),
          ),
        ),
      ],
    );
  }
}
