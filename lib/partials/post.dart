import 'package:flutter/material.dart';
import 'package:localization/localization.dart';

class Post extends StatefulWidget {
  const Post({Key? key}) : super(key: key);

  @override
  State<Post> createState() => _PostState();
}

class _PostState extends State<Post> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const DisplayName(),
        const PostBody(),
        postActionBar(context),
      ],
    );
  }
}

class DisplayName extends StatelessWidget {
  const DisplayName({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const Icon(
          Icons.face,
          size: 64,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "first name display last name name",
              style: Theme.of(context).textTheme.titleMedium,
            ),
            Text(
              "@alice_exampleuser@example.com",
              style: Theme.of(context).textTheme.bodySmall,
            ),
          ],
        ),
      ],
    );
  }
}

class PostBody extends StatefulWidget {
  const PostBody({Key? key}) : super(key: key);

  @override
  State<PostBody> createState() => _PostBodyState();
}

class _PostBodyState extends State<PostBody> {
  bool visible = false;
  String cwButtonText = "show".i18n();
  Icon cwButtonIcon = const Icon(Icons.visibility);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.fromLTRB(0, 6, 0, 6),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
              "sdkfjksdjfkd sldkjfksdjf dskfjsdkfjkds skdfjksdjfisdujfiosdhfjkldsfh sldkfjksdjfksdjfklsdjf"),
          OutlinedButton.icon(
              onPressed: () {
                setState(() {
                  visible = !visible;
                  if (visible) {
                    cwButtonIcon = const Icon(Icons.visibility_off);
                    cwButtonText = "hide".i18n();
                  } else {
                    cwButtonText = "show".i18n();
                    cwButtonIcon = const Icon(Icons.visibility);
                  }
                });
              },
              icon: cwButtonIcon,
              label: Text(cwButtonText)),
          Visibility(
            visible: visible,
            child: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(
                  style: Theme.of(context).textTheme.bodyMedium,
                  text:
                      "Lorem ipsumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Auctor neque vitae tempus quam pellentesque. Scelerisque varius morbi enim nunc faucibus a. Tellus id interdum velit laoreet id donec ultrices. Aliquet bibendum enim facilisis gravida neque convallis a. Massa enim nec dui nunc mattis enim ut tellus. Sed felis eget velit aliquet sagittis id consectetur purus ut. Dignissim convallis aenean et tortor at risus. Integer vitae justo eget magna fermentum iaculis eu non. Ut placerat orci nulla pellentesque dignissim. Nisl suscipit adipiscing bibendum est ultricies. Mi ipsum faucibus vitae aliquet nec ullamcorper sit amet risus."),
            ),
          ),
        ],
      ),
    );
  }
}

Widget postActionBar(context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      IconButton(
        onPressed: () {},
        icon: const Icon(Icons.reply),
      ),
      IconButton(
        onPressed: () {},
        icon: const Icon(Icons.repeat),
      ),
      IconButton(
        onPressed: () {},
        icon: const Icon(Icons.favorite_outline),
      ),
      IconButton(
        onPressed: () {},
        icon: const Icon(Icons.more_horiz),
      )
    ],
  );
}
