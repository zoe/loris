import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:localization/localization.dart';
import 'package:slothmu/partials/main_scaffold.dart';
import 'pages/login.dart';
import 'business_logic/settings.dart' as settings;
import 'package:flutter_localizations/flutter_localizations.dart';
import 'themes/themes.dart' as themes;
import 'global.dart' as global;
import 'business_logic/auth/oauth.dart' as oauth;

String _initRoute = "/";
ThemeData theme = themes.getTheme(themes.available[0]);
Locale activeLocale = const Locale("en");

void main() async {
  Intl.defaultLocale = "en";
  await settings.saveLocale("en");
  activeLocale = await settings.loadLocale();

  // check if all information is available
  if (await settings.loadAuthCode() == "") {
    _initRoute = "/login";
  } else {
    await oauth.refreshToken();
  }
  runApp(const Slothmu());
}

class Slothmu extends StatefulWidget {
  const Slothmu({Key? key}) : super(key: key);

  @override
  State<Slothmu> createState() => _SlothmuState();
}

class _SlothmuState extends State<Slothmu> {
  @override
  Widget build(BuildContext context) {
    LocalJsonLocalization.delegate.directories = ['lib/i18n'];
    return MaterialApp(
      theme: theme,
      locale: activeLocale,
      supportedLocales: global.availableLocales,
      localizationsDelegates: [
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        LocalJsonLocalization.delegate,
      ],
      initialRoute: _initRoute,
      routes: {
        '/': (context) => const MainScaffold(),
        '/login': (context) => const Login(),
      },
    );
  }
}
