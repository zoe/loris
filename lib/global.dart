import 'package:flutter/painting.dart';

const String name = "loris";
const String version = "v0.1 'is this thing on'";
const String useragent = "$name/$version";
const String website = "https://git.kittycat.homes/zoe/slothmu";
const Map<String, String> defaultHeaders = {
  "User-Agent": useragent,
  "accept": "application/json",
  "Content-Type": "application/json"
};
const List<String> bad = ["gab.com", "spinster.xyz", "truthsocial.com"];
const List<Locale> availableLocales = [Locale("en"), Locale("de")];
