bool isValidUsername({required String name}) {
  if (name.isEmpty) {
    return false;
  }
  return RegExp(r".+\@.+\..+").hasMatch(cleanUpUsername(name: name));
}

String cleanUpUsername({required String name}) {
  name = name.replaceAll(" ", "");
  if (name.isNotEmpty) {
    if (name[0] == "@") {
      name = name.substring(1);
    }
  }
  return name;
}

String urlFromUsername({required String name}) {
  name = cleanUpUsername(name: name);
  return name.substring(name.indexOf("@") + 1);
}

String userFromUsername({required String name}) {
  name = cleanUpUsername(name: name);
  return name.substring(0, name.indexOf("@"));
}

// A fully qualified and valid username for example (@)hello@world.com
// The first @ is ignored
class Username {
  late final String user;
  late final String url;
  Username(String username) {
    assert(isValidUsername(name: username));
    user = userFromUsername(name: username);
    url = urlFromUsername(name: username);
  }
}
