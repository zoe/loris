import 'package:flutter/painting.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../global.dart' as global;
import '../business_logic/auth/oauth.dart' as oauth;

enum Settings {
  instanceUrl,
  username,
}

Future<bool> saveInstanceUrl(String url) async {
  final prefs = await SharedPreferences.getInstance();
  return await prefs.setString("instance-url", url);
}

Future<String> loadInstanceUrl() async {
  final prefs = await SharedPreferences.getInstance();
  String? possibleReturn = prefs.getString("instance-url");
  if (possibleReturn == null) {
    return "example.com";
  } else {
    return possibleReturn;
  }
}

Future<bool> saveUsername(String username) async {
  final prefs = await SharedPreferences.getInstance();
  return await prefs.setString("username", username);
}

Future<bool> saveAuthCode(String code) async {
  final prefs = await SharedPreferences.getInstance();
  return await prefs.setString("authcode", code);
}

Future<String> loadAuthCode() async {
  final prefs = await SharedPreferences.getInstance();
  String? code = prefs.getString("authcode");
  if (code == null) {
    return "";
  }
  return code;
}

Future<bool> saveLocale(String locale) async {
  final prefs = await SharedPreferences.getInstance();
  return await prefs.setString("active-locale", locale);
}

Future<Locale> loadLocale() async {
  final prefs = await SharedPreferences.getInstance();
  String? locale = prefs.getString("active-locale");
  if (locale == null) {
    if (global.availableLocales.contains(Locale(Intl.systemLocale))) {
      return Locale(Intl.systemLocale);
    }
    return const Locale("en");
  }
  return Locale(locale);
}

Future<void> saveApp(oauth.App app) async {
  final prefs = await SharedPreferences.getInstance();
  prefs.setString("client-secret", app.clientSecret);
  prefs.setString("client-id", app.clientId);
}

Future<String> loadClientSecret() async {
  final prefs = await SharedPreferences.getInstance();
  final secret = prefs.getString("client-secret");
  if (secret == null) {
    return "";
  } else {
    return secret;
  }
}

Future<String> loadClientId() async {
  final prefs = await SharedPreferences.getInstance();
  final id = prefs.getString("client-id");
  if (id == null) {
    return "";
  } else {
    return id;
  }
}

Future<bool> saveToken(String token) async {
  final prefs = await SharedPreferences.getInstance();
  return await prefs.setString("access-token", token);
}

Future<String> loadToken() async {
  final prefs = await SharedPreferences.getInstance();
  final id = prefs.getString("access-token");
  if (id == null) {
    return "";
  } else {
    return id;
  }
}
