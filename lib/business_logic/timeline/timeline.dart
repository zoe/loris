import 'package:http/http.dart' as http;
import '../settings.dart' as settings;
import '../../global.dart' as global;

Future<http.Response> getTimelineFromServer() async {
  final token = await settings.loadToken();
  final baseUrl = await settings.loadInstanceUrl();
  final url = Uri(
    scheme: "https",
    host: baseUrl,
    path: "/api/v1/timelines/home",
  );

  Map<String, String> headers = {"Authorization": "Bearer $token"};
  headers.addAll(global.defaultHeaders);

  final response = await http.get(url, headers: headers);

  return response;
}
