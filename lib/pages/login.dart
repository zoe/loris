import 'package:flutter/material.dart';
import 'package:localization/localization.dart';
import '../business_logic/auth/oauth.dart' as oauth;
import '../business_logic/settings.dart' as settings;

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Padding(
        padding: EdgeInsets.all(24),
        child: LoginForm(),
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      onChanged: () {
        formKey.currentState!.validate();
      },
      key: formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text("greeting".i18n(), style: Theme.of(context).textTheme.headline1),
          TextFormField(
            onSaved: (value) async {
              await settings.saveInstanceUrl(value!);
            },
            decoration: InputDecoration(
              labelText: "instance-url".i18n(),
              hintText: "instance-url-example".i18n(),
              icon: const Icon(Icons.home),
              prefixText: "https://",
            ),
            autofocus: true,
          ),
          TextButton.icon(
              onPressed: () {
                bool isValid = formKey.currentState!.validate();
                if (!isValid) {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text("login-failed-snackbar-text".i18n())));
                } else {
                  formKey.currentState?.save();
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const AuthPage(),
                      ));
                }
              },
              icon: const Icon(Icons.login),
              label: Text("authorize-in-browser".i18n()))
        ],
      ),
    );
  }
}

/* 
page that handles authenticating user
*/
class AuthPage extends StatefulWidget {
  const AuthPage({Key? key}) : super(key: key);

  @override
  State<AuthPage> createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            FutureBuilder<int>(
              future: oauth.handleFullOauth(),
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return Text("login-failed-snackbar-text".i18n());
                } else if (snapshot.hasData) {
                  if (snapshot.data != null) {
                    if (snapshot.data != 200) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(Icons.error),
                          const SizedBox(
                            width: 24,
                          ),
                          Text("error ${snapshot.data}"),
                        ],
                      );
                    }
                  }
                  return TextButton.icon(
                      onPressed: () {
                        Navigator.pushReplacementNamed(context, "/");
                      },
                      icon: const Icon(Icons.arrow_forward),
                      label: Text("confirm".i18n()));
                } else {
                  return const CircularProgressIndicator();
                }
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TextButton.icon(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: const Icon(Icons.arrow_back),
                    label: Text("back-button".i18n())),
              ],
            )
          ],
        ),
      ),
    );
  }
}
