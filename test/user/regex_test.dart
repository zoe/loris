import 'package:flutter_test/flutter_test.dart';

void main() {
  matchWords();
}

void matchWords() {
  test("try matching some words", () {
    expect(RegExp(r"tiger").hasMatch("tigerfucker"), true);
  });
}
