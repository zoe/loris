import 'package:flutter_test/flutter_test.dart';
import 'package:slothmu/business_logic/user.dart';

void main() {
  validUsernameTest();
  usernameToUrlTest();
  cleanupNameTest();
  userFromUsernameTest();
}

void validUsernameTest() {
  test("test if usernames are valid", () {
    expect(isValidUsername(name: "hello world"), false);
    expect(isValidUsername(name: ""), false);
    expect(isValidUsername(name: "hello@world"), false);
    expect(isValidUsername(name: "@hello@world"), false);
    expect(isValidUsername(name: "@hello@world."), false);
    expect(isValidUsername(name: "@hello@world.example"), true);
    expect(isValidUsername(name: "hello@world.example"), true);
    expect(isValidUsername(name: "noodles.noodles@gts.example.world.example"),
        true);
  });
}

void usernameToUrlTest() {
  test("try valid transforming usernames into a url", () {
    expect(urlFromUsername(name: "hello@example.com"), "example.com");
    expect(urlFromUsername(name: "h@e.c"), "e.c");
  });
}

void cleanupNameTest() {
  test("try cleaning up a username", () {
    expect(cleanUpUsername(name: "@  s o up @  s oup . par ty         "),
        "soup@soup.party");
    expect(cleanUpUsername(name: ""), "");
  });
}

void userFromUsernameTest() {
  test("try extracting user from the full valid user id", () {
    expect(userFromUsername(name: "@hello@world"), "hello");
    expect(userFromUsername(name: "@john@from.garfield"), "john");
  });
}
